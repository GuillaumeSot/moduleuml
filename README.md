# Pire2pire UML
Il fera suite au repo [pire2pire-Merise-BDD](https://gitlab.com/GuillaumeSot/meriseformationmodule), en représentant certaines parties de cette application avec des diagrammes UML:
- Un diagramme de cas d'utilisation
- Un diagramme de classe
- un diagramme d'activité de création de module
- un diagramme de séquence d'inscription à une formation
- un diagramme de séquence de modification de formation

## Table des matières
-   [Definition UML](#definition-uml)
-   [Contexte du projet](#contexte-du-projet)
-   [Critères de performance](#critere-de-performance)
-   [Livrables](#livrables)
-   [Construit avec](#construit-avec)

## Definition UML
Un diagramme UML est un moyen de visualiser des systèmes et des logiciels à l'aide du langage de modélisation unifié (UML). Ces diagrammes permettent aux développeurs ainsi qu'aux autres personnes par forcément du métier de mieux comprendre et de communiquer les aspects essentiels d'un système complexe.

## Contexte du projet

Les formations sont organisés en modules.

Chaque module est caractérisé par un numéro de module sous forme de Semantic Versionning, un intitulé, un objectif pédagogique, un contenu (textes, images et vidéos), une durée en heures, un ou plusieurs tags et un auteur.

Un module peut faire partie d'une ou plusieurs formations, comme par exemple un pire module "Commandes de base Git" pourrait faire partie d'une pire formation "Frontend Javascript" et "DevOps", voir  plus.

Un module peut contenir un texte et/ou une image et/ou une vidéo.

Les apprenants peuvent s'inscrire à une ou plusieurs formations, ils peuvent choisir de ne pas suivre certains des modules s'ils possèdent déjà, par exemple, les compétences. Autrement dit, ils peuvent arbitrairement valider les modules de leur choix en un clic.

Chaque apprenant est évalué pour chaque module et possède un état de fin de module (OK / KO).

Une formation est considérée comme terminée lorsque tous les modules ont été validés.

Chaque apprenant est caractérisé par un numéro d’inscription unique, un nom, un prénom, une adresse et une date de naissance.

Un formateurs est auteur d'un module pour une formation donnée, chaque formateur est caractérisé par un code, un nom, un prénom.

## Critères de performance

- La nomenclature UML est respectée
- Les diagrammes sont cohérents avec la conception de la BDD réalisée avec MERISE
- Les diagrammes sont complets et correctement annotés
- Le format des différents diagrammes est une image accessible sans outils (jpeg, png, etc…)
- Les conventions de commits sont respectées

## Livrables

- Un dépôt Github recensant : 
    - Un README explicite et soigné
    - Un diagramme de classes UML
    - Au moins deux diagrammes de séquence UML différents
    - Un diagramme de cas d'utilisation UML
    - Un diagramme d'activité UML
    - Un document expliquant les choix de conception

## Construit avec

- **StarUML** pour les diagrammes